#include "DebugPrint.hpp"
VOID DbgMsg(LPSTR Format, ...)
{
	CHAR Buffer[512];
	va_list List;

	va_start(List, Format);
	wvsprintfA(Buffer, Format, List);
	va_end(List);

	OutputDebugStringA(Buffer);
}
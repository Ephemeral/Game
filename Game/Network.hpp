#ifndef _NETWORK_HPP_
#define _NETWORK_HPP_
#define WIN32_LEAN_AND_MEAN

#include <Windows.h>
#include <WS2tcpip.h>
#include <MSWSock.h>
#pragma comment(lib,"WS2_32")
BOOLEAN InitializeWSA();
ADDRINFO* CreateEndpointClient(PCSTR Host, PCSTR Port);
SOCKET CreateSocket(ADDRINFO* Endpoint);
BOOLEAN ConnectSocket(SOCKET Connection, ADDRINFO* Endpoint);
BOOLEAN ReadExactly(SOCKET Connection, PCHAR Buffer, ULONG Size);
BOOLEAN WriteExactly(SOCKET Connection, PCHAR Buffer, ULONG Size);
#endif
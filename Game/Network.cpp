#include "Network.hpp"

BOOLEAN InitializeWSA()
{
	WSADATA WSAContext = { 0 };
	if (!WSAStartup(0x202, &WSAContext))
		return TRUE;

	return FALSE;
}

ADDRINFO* CreateEndpointClient(PCSTR Host, PCSTR Port)
{
	ADDRINFO AddressInformation = { 0 };
	ADDRINFO* Endpoint = NULL;

	AddressInformation.ai_family = AF_INET;
	AddressInformation.ai_socktype = SOCK_STREAM;
	AddressInformation.ai_protocol = IPPROTO_TCP;


	if (!getaddrinfo(Host, Port, &AddressInformation, &Endpoint))
		return Endpoint;

	Endpoint = NULL;

	return Endpoint;
}

SOCKET CreateSocket(ADDRINFO* Endpoint)
{
	SOCKET Connection = socket(Endpoint->ai_family, Endpoint->ai_socktype, Endpoint->ai_protocol);
	return Connection;
}

BOOLEAN ConnectSocket(SOCKET Connection, ADDRINFO* Endpoint)
{
	if (!connect(Connection, Endpoint->ai_addr, Endpoint->ai_addrlen))
		return TRUE;

	return FALSE;
}
BOOLEAN ReadExactly(SOCKET Connection, PCHAR Buffer, ULONG Size)
{
	LONG Read = 0;
	do
	{
		LONG Chunk = recv(Connection, (Buffer + Read), Size - Read, 0);
		if (Chunk < 1)
			return FALSE;
		Read += Chunk;
	} while (Read < Size);

	return TRUE;
}
BOOLEAN WriteExactly(SOCKET Connection, PCHAR Buffer, ULONG Size)
{
	LONG Written = 0;
	do
	{
		LONG Chunk = send(Connection, (Buffer + Written), Size - Written, 0);
		if (Chunk < 1)
			return FALSE;
		Written += Chunk;
	} while (Written < Size);

	return TRUE;
}
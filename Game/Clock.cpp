#include "Clock.hpp"

Clock::Clock()
{
	QueryPerformanceFrequency(&Frequency);
	QueryPerformanceCounter(&TimeLastFrame);
}
Clock::~Clock()
{
	Frequency.QuadPart = 0;
}
VOID Clock::NewFrame()
{
	LARGE_INTEGER Time = {0};
	LARGE_INTEGER Delta = {0};

	QueryPerformanceCounter(&Time);
	
	Delta.QuadPart = Time.QuadPart - TimeLastFrame.QuadPart;

	DeltaTime = (FLOAT)Delta.QuadPart / (FLOAT)Frequency.QuadPart;


	TimeLastFrame.QuadPart = Time.QuadPart;
}
FLOAT Clock::TimeElapsedLastFrame()
{
	return DeltaTime;
}
LARGE_INTEGER& Clock::GetFrequency()
{
	return Frequency;
}
void Clock::Start(PLARGE_INTEGER InitialTime)
{
	QueryPerformanceCounter(InitialTime);
}
void Clock::DelayFPS(LONG FramesPerSecond, FLOAT Delta)
{
	_int64 Delay = -((10000000.f / FramesPerSecond) - (Delta * 10000000.f));
	HANDLE Timer = CreateWaitableTimer(0, 1, 0);
	SetWaitableTimer(Timer, (PLARGE_INTEGER)&Delay, 0, 0, 0, 0);
	WaitForSingleObject(Timer, INFINITE);
	CloseHandle(Timer);
}
void Clock::Stop(PLARGE_INTEGER InitialTime, PFLOAT Delta)
{
	LARGE_INTEGER FinalTime = { 0 };
	QueryPerformanceCounter(&FinalTime);
	FinalTime.QuadPart -= InitialTime->QuadPart;
	//FinalTime.QuadPart *= 1000;
	*Delta = (FLOAT)FinalTime.QuadPart / (FLOAT)Frequency.QuadPart;
}
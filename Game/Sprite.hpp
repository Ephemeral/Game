#ifndef _SPRITE_HPP_
#define _SPRITE_HPP_
#include <wincodec.h>
#include <stdint.h>
#include "Graphics.hpp"
#include "DebugPrint.hpp"
#pragma comment(lib,"WindowsCodecs.lib")
class Sprite
{
private:
	ID2D1Bitmap* Bitmap;
	Graphics* GraphicsEngine;
	int32_t SpriteWidth;
	int32_t SpriteHeight;
	int32_t SpritesAcross;

public:
	Sprite(Graphics* GraphicsEngine,LPCTSTR File,int32_t NumberOfSprites);
	~Sprite();
	VOID Draw(FLOAT PositionX,FLOAT PositionY);
	VOID Draw(int32_t Index,FLOAT PositionX,FLOAT PositionY);
private:

};
#endif
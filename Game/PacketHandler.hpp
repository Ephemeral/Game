#ifndef _PACKET_HANDLER_HPP_
#define _PACKET_HANDLER_HPP_
#include <stdint.h>
#include "Network.hpp"
#include "Shared.hpp"
#include "PlayerData.hpp"
#include "Global.hpp"

void HandleIncomingPacket(SOCKET* Connection,ServerPacketHeader* Header);
void HandleSetId(uint32_t MyId);
void HandleConnection(uint32_t ConnectingPlayerId);
void HandleDisconnection(uint32_t DisconnectingPlayerId);
void HandleMove(SOCKET* Connection,uint32_t MovingPlayerId);
void HandleChat();
void HandleAbility();

#endif
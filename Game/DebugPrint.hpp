#ifndef _DEBUGPRINT_HPP_
#define _DEBUGPRINT_HPP_
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
VOID DbgMsg(LPSTR Format, ...);

#endif


#include "Engine.hpp"

Engine::Engine()
{
	MainWindowHandle = NULL;
	RenderTarget = NULL;
	Factory = NULL;
	DWriteFactory = NULL;
	TextFormat = NULL;
	
	WindowDimensions.left = 0;
	WindowDimensions.top = 0;
	WindowDimensions.right = 0;
	WindowDimensions.bottom = 0;
}

BOOLEAN Engine::Initialize(_In_ HRESULT* Result, _In_ HWND MainWindowHandle)
{
	if (!Result || !MainWindowHandle)
		return FALSE;

	if (!GetClientRect(MainWindowHandle, &WindowDimensions))
		return FALSE;

	this->MainWindowHandle = MainWindowHandle;

	*Result = D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED, &Factory);
	if (*Result != S_OK)
		return FALSE;

	*Result = DWriteCreateFactory(DWRITE_FACTORY_TYPE_SHARED, __uuidof(DWriteFactory), reinterpret_cast<IUnknown **>(&DWriteFactory));
	if (*Result != S_OK)
		return FALSE;

	*Result = DWriteFactory->CreateTextFormat(
		L"Arial",
		NULL,
		DWRITE_FONT_WEIGHT_NORMAL,
		DWRITE_FONT_STYLE_NORMAL,
		DWRITE_FONT_STRETCH_NORMAL,
		FONT_SIZE,
		L"",
		&TextFormat
	);
	if (*Result != S_OK)
		return FALSE;
	else
	{
		TextFormat->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_LEADING);
		//TextFormat->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT_CENTER);
	}

	*Result = Factory->CreateHwndRenderTarget(
		D2D1::RenderTargetProperties(D2D1_RENDER_TARGET_TYPE_DEFAULT, D2D1::PixelFormat(DXGI_FORMAT_UNKNOWN, D2D1_ALPHA_MODE_PREMULTIPLIED)),
		D2D1::HwndRenderTargetProperties(MainWindowHandle, D2D1::SizeU(WindowDimensions.right, WindowDimensions.bottom),D2D1_PRESENT_OPTIONS_IMMEDIATELY),
		&RenderTarget);

	if (*Result != S_OK)
	{
		Factory->Release();
		Factory = NULL;
		DWriteFactory->Release();
		DWriteFactory = NULL;
		return FALSE;
	}

	return TRUE;
}

Engine::~Engine()
{
	if (TextFormat)
	{
		TextFormat->Release();
		TextFormat = NULL;
	}
	if (RenderTarget)
	{
		RenderTarget->Release();
		RenderTarget = NULL;
	}
	if (Factory)
	{
		Factory->Release();
		Factory = NULL;
	}
	if (DWriteFactory)
	{
		DWriteFactory->Release();
		DWriteFactory = NULL;
	}
}

ID2D1Factory * Engine::GetEngineFactory()
{
	return Factory;
}

IDWriteFactory * Engine::GetDWriteFactory()
{
	return DWriteFactory;
}

ID2D1RenderTarget * Engine::GetEngineRenderTarget()
{
	return RenderTarget;
}

IDWriteTextFormat* Engine::GetTextFormat()
{
	return TextFormat;
}

VOID Engine::BeginDraw()
{
	 RenderTarget->BeginDraw();
}

VOID Engine::EndDraw()
{
	RenderTarget->EndDraw();
}

RECT * Engine::GetWindowDimensions()
{
	return &WindowDimensions;
}

LONG Engine::GetWIndowWidth()
{
	return WindowDimensions.right;
}

LONG Engine::GetWindowHeight()
{
	return WindowDimensions.bottom;
}

HWND Engine::GetWindowHandle()
{
	return MainWindowHandle;
}

#ifndef _SHARED_HPP_
#define _SHARED_HPP_

#include <stdint.h>
#include <intrin.h>
enum PacketId
{
	MOVE = 0xA,
	ABILITY,
	CHAT,
	SETID,
	CONNECT,
	DISCONNECT
};
struct MovementData
{
	float PositionX;
	float PositionY;
	float VelocityX;
	float VelocityY;
};
struct PacketHeader
{
	uint16_t PacketType;
	uint32_t Checksum;
};
struct ServerPacketHeader
{
	PacketHeader Header;
	uint32_t SenderId;
};
#endif
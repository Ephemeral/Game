#ifndef _GRAPHICS_H_
#define _GRAPHICS_H_



#include "Engine.hpp"


class Graphics : Engine
{
private: 
	Engine* GraphicsEngine;
	ID2D1Brush* Brush;
public:
	Graphics::Graphics(_In_ HWND MainWindowHandle, _Out_ BOOLEAN* Status);
	Graphics::~Graphics();
	ID2D1RenderTarget* GetRenderTarget();
	VOID Graphics::BeginDraw();
	VOID Graphics::EndDraw();
	
	VOID ClearScreen(_In_ FLOAT R, _In_ FLOAT G, _In_ FLOAT B,_In_ FLOAT A);
	VOID ClearScreen(_In_ FLOAT R, _In_ FLOAT G, _In_ FLOAT B);
	VOID ClearScreen();

	VOID CreateBrush(_Out_ ID2D1SolidColorBrush** Brush, _In_ FLOAT R, _In_ FLOAT G, _In_ FLOAT B);

	VOID DrawRectangle(_In_ ID2D1SolidColorBrush * Brush);
	VOID DrawRectangle();

	VOID DrawRoundedRectangle();
	VOID DrawRoundedRectangle(_In_ ID2D1SolidColorBrush * Brush, _In_ D2D1_ROUNDED_RECT* Position);

	VOID CreateRoundedRectangleDimensions(_Out_ D2D1_ROUNDED_RECT* RoundedRectangle,_In_ FLOAT Left,_In_ FLOAT Top,_In_ FLOAT Right,_In_ FLOAT Bottom,_In_ FLOAT RadiusX,_In_ FLOAT RadiusY);
	
	VOID DrawCircle(_In_ FLOAT X, _In_ FLOAT Y, _In_ FLOAT Radius, _In_ FLOAT R, _In_ FLOAT G, _In_ FLOAT B, _In_ FLOAT A);
	VOID OutputText(IN ID2D1SolidColorBrush * Brush, IN WCHAR* Text, IN DWORD_PTR TextSize, IN FLOAT X, IN FLOAT Y);
	VOID DrawCircle(_In_ ID2D1SolidColorBrush * Brush, _In_ FLOAT X, _In_ FLOAT Y, _In_ FLOAT Radius);
	RECT* GetWindowDimensions();

	VOID DrawCircularLoadingBar(_In_ ID2D1SolidColorBrush* Brush, _In_ FLOAT X, _In_ FLOAT Y, _In_ FLOAT Radius, _In_ FLOAT Progress);
	VOID DrawEqualLengthTriangle(_In_ ID2D1SolidColorBrush* Brush, _In_ FLOAT X, _In_ FLOAT Y,_In_ FLOAT Distance);
	VOID DrawEqualLengthTriangle(_In_ ID2D1SolidColorBrush* Brush, _In_ FLOAT X, _In_ FLOAT Y,_In_ FLOAT Distance,_In_ FLOAT Stroke);

	VOID Graphics::DrawLine(_In_ ID2D1SolidColorBrush* Brush, _In_ FLOAT XInitial, _In_ FLOAT YInitial, _In_ FLOAT XFinal, _In_ FLOAT YFinal);
	VOID SetView(FLOAT ViewX,FLOAT ViewY);
	VOID Graphics::ResetView();
	HWND GetWindowHandle();

};
#endif
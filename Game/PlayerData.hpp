#ifndef _PLAYERDATA_HPP_
#define _PLAYERDATA_HPP_
#include "Shared.hpp"
#include "Constants.h"

class PlayerData
{
public:
	PlayerData(float PositionX,float PositionY)
	{
		PlayerData(PositionX,PositionY,0.0f,0.0f);
	}
	PlayerData(float PositionX,float PositionY, float VelocityX,float VelocityY)
	{
		PlayerData(PositionX,PositionY,VelocityX,VelocityY,0);
	}
	PlayerData(float PositionX,float PositionY, float VelocityX,float VelocityY, uint32_t PlayerID)
	{
		MoveData.PositionX = PositionX;
		MoveData.PositionY = PositionY;
		MoveData.VelocityX = VelocityX;
		MoveData.VelocityY = VelocityY;
		this->PlayerID = PlayerID;
		//FirstMove = 0;
		IsTouchingSurface = 1;
	}
	~PlayerData()
	{

	}
	//void InterpolateMovement(float Factor)
	//{
	//	MoveData.PositionX += Factor * (NewMoveData.PositionX - MoveData.PositionX);
	//	MoveData.PositionY += Factor * (NewMoveData.PositionY - MoveData.PositionY);

	//	MoveData.VelocityX += Factor * (NewMoveData.VelocityX - MoveData.VelocityX);
	//	MoveData.VelocityY += Factor * (NewMoveData.VelocityY - MoveData.VelocityY);
	//}
	void CalculateMovement(float DeltaTime)
	{
		if (MoveData.PositionY >= ((HEIGHT - RADIUS) - MoveData.VelocityY * DeltaTime))
		{
			MoveData.PositionY = (HEIGHT - RADIUS);
			MoveData.VelocityY = 0;
			IsTouchingSurface = 1;
		}
		else
		{
			MoveData.VelocityY += GRAVITY_CONST * DeltaTime;
			IsTouchingSurface = 0;
		}
		MoveData.PositionX += MoveData.VelocityX * DeltaTime;
		MoveData.PositionY += MoveData.VelocityY * DeltaTime;
	}
	int8_t IsTouchingSurface;
	uint32_t PlayerID;
	MovementData MoveData;
	//MovementData NewMoveData;
	//uint8_t FirstMove;
};

#endif
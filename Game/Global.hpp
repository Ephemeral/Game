#ifndef _GLOBAL_HPP_
#define _GLOBAL_HPP_
#include <stdint.h>
#include <vector>
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

#include <boost\shared_ptr.hpp>
#include <algorithm>
#include "DebugPrint.hpp"
#include "Network.hpp"
#include "PlayerData.hpp"


using std::vector;
typedef boost::shared_ptr<PlayerData> PlayerDataPtr;
extern uint8_t KeyState[4];
extern std::vector<PlayerDataPtr> Players;
extern PlayerDataPtr Self;
extern CRITICAL_SECTION Lock;
extern BOOLEAN Connected;

enum Key
{
	LEFT,
	RIGHT,
	UP,
	DOWN,
	SPACE
};



ULONG CALLBACK ReadThread(PVOID Context);

LRESULT CALLBACK WindowProc(HWND WindowHandle, UINT Msg, WPARAM WideParam, LPARAM LongParam);

SOCKET InitializeConnection(LPSTR Host,LPSTR Port);

#endif
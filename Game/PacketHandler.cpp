#include "PacketHandler.hpp"

void HandleIncomingPacket(SOCKET* Connection,ServerPacketHeader* Header)
{
	switch(Header->Header.PacketType)
	{
	case PacketId::SETID:
		HandleSetId(Header->SenderId);
		break;

	case PacketId::CONNECT:
		HandleConnection(Header->SenderId);
		break;

	case PacketId::DISCONNECT:
		HandleDisconnection(Header->SenderId);
		break;

	case PacketId::MOVE:
		HandleMove(Connection,Header->SenderId);
		break;

	case PacketId::CHAT:
		HandleChat();
		break;

	case PacketId::ABILITY:
		HandleAbility();
		break;

	default:
		Connected = FALSE;
		closesocket(*Connection);
		*Connection = INVALID_SOCKET;
		break;
	}
}
void HandleSetId(uint32_t MyId)
{
	Self->PlayerID = MyId;
}
void HandleConnection(uint32_t ConnectingPlayerId)
{
	PlayerDataPtr NewPlayer(new PlayerData(0.0f, 0.0f));
	NewPlayer->PlayerID = ConnectingPlayerId;
	EnterCriticalSection(&Lock);
	Players.push_back(NewPlayer);
	LeaveCriticalSection(&Lock);
}
void HandleDisconnection(uint32_t DisconnectingPlayerId)
{
	/*Players.erase
	(
		std::find_if(Players.begin(), Players.end(),[DisconnectingPlayerId](const PlayerDataPtr& Player)
		{
			return Player->PlayerID == DisconnectingPlayerId;
		})
	);*/
	for (int Index = 0; Index < Players.size(); ++Index)
	{
		if (Players.at(Index)->PlayerID == DisconnectingPlayerId)
		{
			EnterCriticalSection(&Lock);
			Players.erase(Players.begin() + Index);
			LeaveCriticalSection(&Lock);
		}
	}

}
void HandleMove(SOCKET* Connection,uint32_t MovingPlayerId)
{
	MovementData PlayerUpdate;
	if (!ReadExactly(*Connection, (PCHAR)&PlayerUpdate, sizeof(MovementData)))
	{
		Connected = FALSE;
		closesocket(*Connection);
		*Connection = INVALID_SOCKET;
		return;
	}

	for (PlayerDataPtr Opponent : Players)
	{
		if (Opponent->PlayerID == MovingPlayerId)
		{
			memcpy(&Opponent->MoveData, &PlayerUpdate, sizeof(MovementData));

			//memcpy(&Opponent->NewMoveData, &PlayerUpdate, sizeof(MovementData));
			//printf("Player %i moved to x: %f y: %f\n", Opponent.PlayerID, Opponent.MoveData.PositionX, Opponent.MoveData.PositionY);
			break;
		}
	}
	//auto Found = [MovingPlayerId](const PlayerDataPtr& Player)
	//{
	//	return Player->PlayerID == MovingPlayerId;
	//};
	//vector<PlayerDataPtr>::iterator Target = std::find_if(Players.begin(), Players.end(), Found);

	//PlayerDataPtr Player = *Target;

	//if (Player->PlayerID == MovingPlayerId)//Safety check
	//{
	//	memcpy(&Player->NewMoveData,&PlayerUpdate, sizeof(MovementData));
	//}
}
void HandleChat()
{
}
void HandleAbility()
{
}
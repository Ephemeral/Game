#ifndef _CONSTANTS_H_
#define _CONSTANTS_H_

#define HEIGHT 1024
#define WIDTH 1024
#define MAX_VELOCITYX 350.0f
#define GRAVITY_CONST 700.f
#define ACCELERATION_CONST 800.f
#define FRICTION_CONST 500.f
#define JUMPFORCE_CONST 500.f
#define RADIUS 100.0f

#endif

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <vector>

#include "Graphics.hpp"
#include "Network.hpp"
#include "Clock.hpp"
#include "PlayerData.hpp"
#include "Global.hpp"
#include "PacketHandler.hpp"
#include "Sprite.hpp"

SOCKET Connection = INVALID_SOCKET;
Sprite *Background = NULL, *Character = NULL;
VOID GameLoop(Graphics* GraphicsEngine);
uint8_t Done = 0;
ULONG Frame;
BOOL WINAPI WinMain(HINSTANCE Current, HINSTANCE Old, PCHAR CmdLine, INT CmdShow)
{
    /*
	WNDCLASSEXW WindowClass = { 0 };
	RECT WindowDimensions = { 0 };

	HWND WindowHandle = NULL;
	HWND ChildWindowHandle = NULL;
	Graphics* GraphicsEngine = NULL;

	BOOLEAN Status = FALSE;

	Self = PlayerDataPtr(new PlayerData(0.0f, 0.0f));
	Self->PlayerID = 0;
	Players.push_back(Self);

	WindowClass.cbSize = sizeof(WNDCLASSEXW);
	WindowClass.lpszClassName = L"Rotation";
	WindowClass.hInstance = Current;
	WindowClass.hbrBackground = (HBRUSH)COLOR_WINDOW;
	WindowClass.style = CS_HREDRAW | CS_VREDRAW;
	WindowClass.lpfnWndProc = WindowProc;


	RegisterClassExW(&WindowClass);
	WindowDimensions.bottom = 1024;
	WindowDimensions.right = 1024;
	AdjustWindowRect(&WindowDimensions, WS_OVERLAPPEDWINDOW, FALSE);
	WindowHandle = CreateWindowExW(WS_EX_OVERLAPPEDWINDOW, L"Rotation", L"Client", WS_OVERLAPPEDWINDOW, 0, 0,
		WindowDimensions.right,
		WindowDimensions.bottom, NULL, NULL, Current, NULL);

	SetCursor(NULL);

	GraphicsEngine = new Graphics(WindowHandle, &Status);
	if (!Status)
	{
		delete GraphicsEngine;
		DestroyWindow(WindowHandle);
		return FALSE;
	}

	ShowWindow(WindowHandle, CmdShow);

	Background = new Sprite(GraphicsEngine, L"Resources\\Background.bmp", 1);
	Character = new Sprite(GraphicsEngine, L"Resources\\Stick.bmp", 2);
	GameLoop(GraphicsEngine);
	Players.clear();
	WSACleanup();

	//delete Character;
	//delete Background;
	
	delete GraphicsEngine;

	DestroyWindow(WindowHandle);

	return TRUE;
	*/
}
BOOLEAN WriteMovement(SOCKET* Connection)
{
	PacketHeader Header = { 0 };
	Header.PacketType = PacketId::MOVE;
	if (!WriteExactly(*Connection, (PCHAR)&Header, sizeof(PacketHeader)))
	{
		return FALSE;
	}

	if (!WriteExactly(*Connection, (PCHAR)&Self->MoveData, sizeof(MovementData)))
	{
		return FALSE;
	}
	return TRUE;
}
ULONG CALLBACK ConnectThread(LPVOID Context)
{
	SOCKET* Connection = (SOCKET*)Context;
	while(!Done)
	{
		if(!Connected)
		{
			Players.clear();
			EnterCriticalSection(&Lock);
			Players.push_back(Self);
			LeaveCriticalSection(&Lock);
			*Connection = InitializeConnection("127.0.0.1","8080");
		}
		Sleep(100);
	}
	return 0;
}
VOID GameLoop(Graphics* GraphicsEngine)
{
    /*
	MSG	Message = { 0 };
	Clock Timer;
	LARGE_INTEGER Frequency = { 0 };

	HANDLE ThreadHandle = NULL;
	
	int32_t Index = 0;

	HANDLE ConnectionThread = NULL;

	InitializeCriticalSection(&Lock);

	QueryPerformanceFrequency(&Frequency);

	ThreadHandle = CreateThread(0,0,ReadThread,&Connection,0,0);
	
	ConnectionThread = CreateThread(NULL,0,ConnectThread,&Connection,0,0);

	//WCHAR Disconnected[] = L"Disconnected\0\0";
	//WCHAR Connected[] = L"Disconnected\0\0";
	//WCHAR* Status = NULL;

	while (!Done)
	{
		static uint32_t Update = 0;
		FLOAT DeltaTime = 0.0f;
		FLOAT Previous = 0.0f;
		FLOAT ScrollX = 0.0f;
		FLOAT ScrollY = 0.0f;

		ID2D1SolidColorBrush* Brush = NULL;

		LARGE_INTEGER Initial = { 0 };
		LARGE_INTEGER Final = { 0 };
		//Timer.Start(&Initial);
		QueryPerformanceCounter(&Initial);

		Timer.NewFrame();
		DeltaTime = Timer.TimeElapsedLastFrame();

		while (PeekMessageW(&Message, NULL, 0, 0, PM_REMOVE))
		{

			TranslateMessage(&Message);
			DispatchMessageW(&Message);
			if (Message.message == WM_QUIT)
			{
				Done = 1;
				break;
			}
		}
		if (Done)
			break;

		if (KeyState[LEFT])
		{
			Self->MoveData.VelocityX -= ACCELERATION_CONST * DeltaTime;
		}
		if (KeyState[RIGHT])
		{
			Self->MoveData.VelocityX += ACCELERATION_CONST * DeltaTime;
		}
		if (!KeyState[LEFT] && Self->MoveData.VelocityX < 0)
			Self->MoveData.VelocityX = min(Self->MoveData.VelocityX + FRICTION_CONST * DeltaTime, 0.f);
		if (!KeyState[RIGHT] && Self->MoveData.VelocityX > 0)
			Self->MoveData.VelocityX = max(Self->MoveData.VelocityX - FRICTION_CONST * DeltaTime, 0.f);

		Self->MoveData.VelocityX = max(-MAX_VELOCITYX, min(MAX_VELOCITYX, Self->MoveData.VelocityX));
		
		if(Connected)
		{
			WriteMovement(&Connection);
		}

		GraphicsEngine->CreateBrush(&Brush, 1.0f, 0.5f, 1.0f);

		GraphicsEngine->BeginDraw();

		GraphicsEngine->ClearScreen(0.0f,0.0f,0.0f);


		ScrollX = WIDTH / 2 - 100 / 2 - Self->MoveData.PositionX;
		ScrollY = HEIGHT / 2 - 100 / 2 - Self->MoveData.PositionY;

		
		GraphicsEngine->SetView(ScrollX, ScrollY);

		Background->Draw(0.0f,0.0f);

		for (PlayerDataPtr Player : Players)
		{
			//if (Player != Self)
			//{
			//	float dif = abs(sqrt(pow((Player->NewMoveData.PositionX - Player->MoveData.PositionX), 2) + pow((Player->NewMoveData.PositionY - Player->MoveData.PositionY), 2)));

			//	if (!Player->FirstMove)
			//	{
			//		Player->FirstMove = 1;
			//		Player->InterpolateMovement(1.0f);
			//	}
			//	if (dif <= 1.f)
			//		Player->InterpolateMovement(1.0f);
			//	else if (dif > 500.f)
			//		Player->InterpolateMovement(0.9f);
			//	else if (dif < 200.f && dif > 1.f)
			//		Player->InterpolateMovement(0.1f);
			//	else
			//		Player->InterpolateMovement(0.05f);
			//}

			//if (Player == Self)
			//{
			//	
			//}

			EnterCriticalSection(&Lock);
			if (!Player)
			{
				LeaveCriticalSection(&Lock);

				continue;

			}
			Player->CalculateMovement(DeltaTime);
			LeaveCriticalSection(&Lock);//I might move this to the bottom of the loop if any problems occur later
										//But it is working fine now, so no need

			if (Player->IsTouchingSurface)
				Frame = 0;
			else
				Frame = 1;
			Character->Draw(Frame,Player->MoveData.PositionX,Player->MoveData.PositionY);
			//GraphicsEngine->DrawCircle(Brush,Player->MoveData.PositionX,Player->MoveData.PositionY,RADIUS);
			WCHAR buf[16] = {0};
			_itow(Player->PlayerID, buf, 10);
			if(Connected)
				GraphicsEngine->OutputText(Brush, buf, wcslen(buf) * 2, Player->MoveData.PositionX, Player->MoveData.PositionY);

		}

		GraphicsEngine->DrawCircle(Brush, 600, 400, 200);
		GraphicsEngine->ResetView();
		
		WCHAR buf[32] = {0};
		wsprintf(buf,L"Players: %i", (int)Players.size());
		GraphicsEngine->OutputText(Brush, buf, wcslen(buf) * 2, 100, 100);

		// Draw static objects between ResetView and EndDraw
		GraphicsEngine->EndDraw();

		Brush->Release();

		QueryPerformanceCounter(&Final);
		FLOAT Time = ((FLOAT)(Final.QuadPart - Initial.QuadPart) / Frequency.QuadPart);
		_int64 Delay = (_int64)(-((10000000.f / 60) - (Time * 10000000.f)));

		HANDLE Timer = CreateWaitableTimerW(0, 1, 0);
		SetWaitableTimer(Timer, (PLARGE_INTEGER)&Delay, 0, 0, 0, 0);
		WaitForSingleObject(Timer, INFINITE);
		CloseHandle(Timer);
	}
	TerminateThread(ConnectionThread,0);
	CloseHandle(ConnectionThread);
	EnterCriticalSection(&Lock);
	TerminateThread(ThreadHandle,0);
	CloseHandle(ThreadHandle);
	LeaveCriticalSection(&Lock);
	DeleteCriticalSection(&Lock);
	*/
}

LRESULT CALLBACK WindowProc(HWND WindowHandle, UINT Msg, WPARAM WideParam, LPARAM LongParam)
{
	BOOLEAN NewState = FALSE;

	switch (Msg)
	{
	case WM_CLOSE:
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	case WM_KEYUP:
	case WM_KEYDOWN:
		{
			Msg == WM_KEYDOWN ? NewState = TRUE : NewState = FALSE;
			switch(WideParam)
			{
			case VK_SPACE:
				if(Self->MoveData.PositionY == (HEIGHT - RADIUS))
				{
					Self->MoveData.VelocityY = - JUMPFORCE_CONST;
				}
				break;
			case VK_LEFT:
				KeyState[LEFT] = NewState;
				break;
			case VK_RIGHT:
				KeyState[RIGHT] = NewState;
				break;

			}
		}
		break;
	case WM_SETCURSOR:
		{
			if (LOWORD(LongParam) == HTCLIENT)
				SetCursor(NULL);
		}
		break;
	default:
		{

		}
	}
	return DefWindowProcW(WindowHandle, Msg, WideParam, LongParam);
}

ULONG CALLBACK ReadThread(PVOID Context)
{
	SOCKET* Connection = (SOCKET*)Context;
	while (!Done)
	{
		if(Connected)
		{
			ServerPacketHeader Header = { 0 };

			if (!ReadExactly(*Connection, (PCHAR)&Header, sizeof(ServerPacketHeader)))
			{
				Connected = FALSE;
				closesocket(*Connection);
				*Connection = INVALID_SOCKET;
			}
			else
				HandleIncomingPacket(Connection,&Header);
		}
		else
			Sleep(100);
	}
	return 1;
}
SOCKET InitializeConnection(LPSTR Host,LPSTR Port)
{
	ADDRINFO* Endpoint = NULL;
	Connection = INVALID_SOCKET;
	if(!InitializeWSA())
		goto Cleanup;
	if (!(Endpoint = CreateEndpointClient(Host,Port)))
		goto Cleanup;
	if ((Connection = CreateSocket(Endpoint)) == INVALID_SOCKET)
		goto Cleanup;
	if (ConnectSocket(Connection, Endpoint))
	{
		freeaddrinfo(Endpoint);
		Endpoint = NULL;
		Connected = TRUE;
		return Connection;
	}
Cleanup:
	if(Endpoint)
		freeaddrinfo(Endpoint);
	if (Connection != INVALID_SOCKET)
	{
		closesocket(Connection);
		Connection = INVALID_SOCKET;
	}
	return INVALID_SOCKET;
}
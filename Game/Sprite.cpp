#include "Sprite.hpp"

Sprite::Sprite(Graphics* GraphicsEngine,LPCTSTR File,int32_t NumberOfSprites)
{
	Bitmap = NULL;
	HRESULT Result = 0;//Unused
	IWICBitmapDecoder* Decoder = NULL;
	IWICImagingFactory* Factory = NULL;
	IWICBitmapFrameDecode* Frame = NULL;
	IWICFormatConverter* Converter = NULL;

	this->GraphicsEngine = GraphicsEngine;
	Bitmap = NULL;

	Result = CoCreateInstance
		(
			CLSID_WICImagingFactory,
			NULL,
			CLSCTX_INPROC_SERVER,
			IID_IWICImagingFactory,
			(LPVOID*)&Factory
		);
	if(FAILED(Result))
		DbgMsg("Line:%lu failed with : %X\n",__LINE__,HIWORD(Result));
	Result = Factory->CreateDecoderFromFilename
		(
			File,
			NULL,
			GENERIC_READ,
			WICDecodeMetadataCacheOnLoad,
			&Decoder
		);
	if(FAILED(Result))
		DbgMsg("Line:%lu failed with : %X\n",__LINE__,HIWORD(Result));
	Result = Decoder->GetFrame
		(
			0,
			&Frame
		);
	if(FAILED(Result))
		DbgMsg("Line:%lu failed with : %X\n",__LINE__,HIWORD(Result));
	Result = Factory->CreateFormatConverter(&Converter);
	if(FAILED(Result))
		DbgMsg("Line:%lu failed with : %X\n",__LINE__,HIWORD(Result));
	Result = Converter->Initialize
		(
			Frame,
			GUID_WICPixelFormat32bppPBGRA,
			WICBitmapDitherTypeNone,
			NULL,
			0.0f,
			WICBitmapPaletteTypeCustom
		);
	if(FAILED(Result))
		DbgMsg("Line:%lu failed with : %X\n",__LINE__,HIWORD(Result));
	Result = GraphicsEngine->GetRenderTarget()->CreateBitmapFromWicBitmap
		(
			Converter,
			&Bitmap
		);
	if(FAILED(Result))
		DbgMsg("Line:%lu failed with : %X\n",__LINE__,HIWORD(Result));
	Factory->Release();
	Decoder->Release();
	Converter->Release();
	Frame->Release();

	SpriteWidth = (int32_t)Bitmap->GetSize().width / NumberOfSprites;
	SpriteHeight = (int32_t)Bitmap->GetSize().height;
	SpritesAcross = NumberOfSprites;
}
VOID Sprite::Draw(FLOAT PositionX,FLOAT PositionY)
{
	Draw(0,PositionX,PositionY);
}
VOID Sprite::Draw(int32_t Index,FLOAT PositionX,FLOAT PositionY)
{
	D2D1_RECT_F Source = D2D1::RectF
		(
			(FLOAT)((Index % SpritesAcross) * SpriteWidth),
			(FLOAT)((Index / SpritesAcross) * SpriteWidth),
			(FLOAT)((Index % SpritesAcross) * SpriteWidth) + SpriteWidth,
			(FLOAT)((Index / SpritesAcross) * SpriteWidth) + SpriteHeight
		);
	D2D1_RECT_F Destination = D2D1::RectF
		(
			PositionX,
			PositionY,
			PositionX+SpriteWidth,
			PositionY+SpriteHeight
		);
	GraphicsEngine->GetRenderTarget()->DrawBitmap
		(
			Bitmap,
			Destination,
			1.0f,
			D2D1_BITMAP_INTERPOLATION_MODE::D2D1_BITMAP_INTERPOLATION_MODE_NEAREST_NEIGHBOR,
			Source
		);
}
Sprite::~Sprite()
{
	if(Bitmap)
	{
		//MessageBox(0, L"Sprite released", 0, 0);
		Bitmap->Release();
	}
}
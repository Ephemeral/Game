#ifndef _CLOCK_HPP_
#define _CLOCK_HPP_
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <stdint.h>
class Clock
{
private:
	LARGE_INTEGER Frequency;
	LARGE_INTEGER TimeLastFrame;
	LARGE_INTEGER DeltaLastFrame;
	FLOAT DeltaTime;
public:

	Clock();
	~Clock();
	VOID NewFrame();
	FLOAT TimeElapsedLastFrame();
	LARGE_INTEGER& GetFrequency();
	void Start(PLARGE_INTEGER Initial);
	void Stop(PLARGE_INTEGER InitialTime, PFLOAT Delta);
	void DelayFPS(LONG FramesPerSecond, FLOAT Delta);
};
#endif



#include "Graphics.hpp"

Graphics::Graphics(_In_ HWND MainWindowHandle, _Out_ BOOLEAN* Status)
{
	GraphicsEngine = NULL;

	if (Status)
	{
		HRESULT Result = S_FALSE;
		
		*Status = FALSE;

		if (MainWindowHandle)
		{
			GraphicsEngine = new Engine();

			if (!GraphicsEngine->Initialize(&Result, MainWindowHandle))
			{
				delete GraphicsEngine;
				GraphicsEngine = NULL;
			}
			else
				*Status = TRUE;
		}
	}
}

Graphics::~Graphics()
{
	if (GraphicsEngine)
	{
		delete GraphicsEngine;
		GraphicsEngine = NULL;
	}
}

VOID Graphics::BeginDraw()
{
	GraphicsEngine->BeginDraw();
}

VOID Graphics::EndDraw()
{
	GraphicsEngine->EndDraw();
}

VOID Graphics::ClearScreen()
{
	ClearScreen(1.0f, 1.0f, 1.0f);
}
VOID Graphics::ClearScreen(_In_ FLOAT R, _In_ FLOAT G, _In_ FLOAT B)
{
	ClearScreen(R, G, B, 1.0f);
}
VOID Graphics::ClearScreen(_In_ FLOAT R, _In_ FLOAT G, _In_ FLOAT B, _In_ FLOAT A )
{
	GraphicsEngine->GetEngineRenderTarget()->Clear(D2D1::ColorF(R, G, B, A));
}
VOID Graphics::DrawRectangle()
{

}

VOID Graphics::DrawRectangle(_In_ ID2D1SolidColorBrush * Brush)
{

}

VOID Graphics::DrawRoundedRectangle()
{

}
VOID Graphics::CreateBrush(_Out_ ID2D1SolidColorBrush** Brush, _In_ FLOAT R, _In_ FLOAT G, _In_ FLOAT B)
{
	GraphicsEngine->GetEngineRenderTarget()->CreateSolidColorBrush(D2D1::ColorF(R, G, B), Brush);
}

VOID Graphics::DrawRoundedRectangle(_In_ ID2D1SolidColorBrush * Brush,_In_ D2D1_ROUNDED_RECT* Position  )
{
	GraphicsEngine->GetEngineRenderTarget()->DrawRoundedRectangle(Position, Brush);
}

VOID Graphics::CreateRoundedRectangleDimensions(_Out_ D2D1_ROUNDED_RECT * RoundedRectangle,_In_ FLOAT Left,_In_ FLOAT Top,_In_ FLOAT Right,_In_ FLOAT Bottom,_In_ FLOAT RadiusX,_In_ FLOAT RadiusY)
{
	*RoundedRectangle = D2D1::RoundedRect(D2D1::RectF(Left, Top, Right, Bottom), RadiusX, RadiusY);
}


VOID Graphics::DrawCircle(_In_ FLOAT X,_In_ FLOAT Y,_In_ FLOAT Radius,_In_ FLOAT R,_In_ FLOAT G,_In_ FLOAT B, _In_ FLOAT A)
{
	

}

VOID Graphics::DrawCircle(_In_ ID2D1SolidColorBrush * Brush, _In_ FLOAT X, _In_ FLOAT Y, _In_ FLOAT Radius)
{
	GraphicsEngine->GetEngineRenderTarget()->DrawEllipse(D2D1::Ellipse(D2D1::Point2F(X, Y), Radius, Radius), Brush, 3.0f);
}

VOID Graphics::OutputText(IN ID2D1SolidColorBrush * Brush, IN WCHAR* Text, IN DWORD_PTR TextSize, IN FLOAT X, IN FLOAT Y)
{
	IDWriteTextLayout* TextLayout = NULL;
	GraphicsEngine->GetDWriteFactory()->CreateTextLayout(Text, TextSize, GraphicsEngine->GetTextFormat(), 0, 0, &TextLayout);
	FLOAT MinWidth = 0;
	TextLayout->DetermineMinWidth(&MinWidth);
	TextLayout->SetMaxWidth(MinWidth);
	TextLayout->SetMaxHeight(FONT_SIZE);
	GraphicsEngine->GetEngineRenderTarget()->DrawTextLayout(D2D1::Point2F(X-(MinWidth/2), Y-(FONT_SIZE/2)), TextLayout, Brush);
	TextLayout->Release();
}

RECT * Graphics::GetWindowDimensions()
{
	return GraphicsEngine->GetWindowDimensions();
}

VOID Graphics::DrawCircularLoadingBar(_In_ ID2D1SolidColorBrush * Brush,_In_ FLOAT X,_In_ FLOAT Y,_In_ FLOAT Radius,_In_ FLOAT Progress)
{
	//ID2D1PathGeometry* PathGeometry = NULL;
	//ID2D1GeometrySink* GeometrySink = NULL;

	//GraphicsEngine->GetEngineRenderTarget()->DrawEllipse(D2D1::Ellipse(D2D1::Point2F(X, Y), Radius, Radius), Brush, 3.0f);
	//GraphicsEngine->GetEngineRenderTarget()->DrawEllipse(D2D1::Ellipse(D2D1::Point2F(X, Y), Radius+25, Radius+25), Brush, 3.0f);
	//GraphicsEngine->GetEngineFactory()->CreatePathGeometry(&PathGeometry);
	//PathGeometry->Open(&GeometrySink);
	//GeometrySink->
	//GeometrySink->Release();
	//PathGeometry->Release();
}
VOID Graphics::DrawLine(_In_ ID2D1SolidColorBrush* Brush, _In_ FLOAT XInitial, _In_ FLOAT YInitial, _In_ FLOAT XFinal, _In_ FLOAT YFinal)
{
	GraphicsEngine->GetEngineRenderTarget()->DrawLine(D2D1::Point2F(XInitial, YInitial), D2D1::Point2F(XFinal, YFinal), Brush, 3.0f);
	
}

HWND Graphics::GetWindowHandle()
{
	return GraphicsEngine->GetWindowHandle();
}

VOID Graphics::DrawEqualLengthTriangle(_In_ ID2D1SolidColorBrush* Brush, _In_ FLOAT X, _In_ FLOAT Y, _In_ FLOAT Distance)
{
	DrawEqualLengthTriangle(Brush, X, Y, Distance, 3.0f);
}
VOID Graphics::DrawEqualLengthTriangle(_In_ ID2D1SolidColorBrush* Brush, _In_ FLOAT X, _In_ FLOAT Y,_In_ FLOAT Distance, _In_ FLOAT Stroke)
{
	GraphicsEngine->GetEngineRenderTarget()->DrawLine(D2D1::Point2F(X, Y), D2D1::Point2F(X + Distance, Y + Distance), Brush, Stroke);
	GraphicsEngine->GetEngineRenderTarget()->DrawLine(D2D1::Point2F(X+ Distance, Y+ Distance), D2D1::Point2F(X - Distance, Y+ Distance), Brush, Stroke);
	GraphicsEngine->GetEngineRenderTarget()->DrawLine(D2D1::Point2F(X - Distance, Y + Distance), D2D1::Point2F(X , Y), Brush, Stroke);
}

VOID Graphics::SetView(FLOAT ViewX,FLOAT ViewY)
{
	GraphicsEngine->GetEngineRenderTarget()->SetTransform(D2D1::Matrix3x2F::Translation(ViewX,ViewY));
}

VOID Graphics::ResetView()
{
	GraphicsEngine->GetEngineRenderTarget()->SetTransform(D2D1::Matrix3x2F::Identity());
}
ID2D1RenderTarget* Graphics::GetRenderTarget()
{
	return GraphicsEngine->GetEngineRenderTarget();
}
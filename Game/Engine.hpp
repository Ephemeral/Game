#ifndef _ENGINE_H_
#define _ENGINE_H_



#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <d2d1.h>
#include <dwrite.h>
#pragma comment(lib,"d2d1")
#pragma comment(lib,"Dwrite.lib")

#define FONT_SIZE 50.f

class Engine
{
private:
	HWND MainWindowHandle;
	ID2D1Factory* Factory;
	IDWriteFactory* DWriteFactory;
	ID2D1HwndRenderTarget* RenderTarget;
	IDWriteTextFormat* TextFormat;
	RECT WindowDimensions;
public:
	Engine::Engine();
	Engine::~Engine();
	BOOLEAN Initialize(_In_ HRESULT* Result, _In_ HWND MainWindowHandle);
	
	ID2D1Factory* GetEngineFactory();
	IDWriteFactory* GetDWriteFactory();
	ID2D1RenderTarget* GetEngineRenderTarget();
	IDWriteTextFormat* GetTextFormat();
	VOID BeginDraw();  
	VOID EndDraw();
	RECT* GetWindowDimensions();
	LONG GetWIndowWidth();
	LONG GetWindowHeight();
	HWND GetWindowHandle();
};
#endif